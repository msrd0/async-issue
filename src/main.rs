#![allow(dead_code)]

use futures_util::future::FutureExt;
use gotham_restful::{NoContent, Resource, ResourceMethod, ResourceReadAll, State};
use std::{
	future::Future,
	pin::Pin
};

#[derive(Resource)]
struct TestResource;

async fn do_sth(_state : &State) -> NoContent
{
	Default::default()
}

impl ResourceMethod for TestResource
{
	type Res = NoContent;
}

impl ResourceReadAll for TestResource
{
	fn read_all(state : State) -> Pin<Box<dyn Future<Output = (State, NoContent)> + Send>>
	{
		async move {
			let res = do_sth(&state).await;
			(state, res)
		}.boxed()
	}
}

// irrelevant
fn main()
{
	unimplemented!()
}
